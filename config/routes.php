<?php

return [
    'admin'                         => 'admin/default/index',
    'admin/<controller>/<action>'   => 'admin/<controller>/<action>',

    '/'                             => 'news/index',
    '/news/<slug:[a-zA-Z0-9-]+>'    => 'news/view',
    '/news/category/<id:\d+>'       => 'news/category',
    '/news/comment/<id:\d+>'        => 'news/comment',
    '<controller>/<action>'         => '<controller>/<action>',
];