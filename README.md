Требования
==========
- PHP 5.4
- MySQL
- Apache 2.4

Установка
=========
Установить `composer`
```
curl -sS https://getcomposer.org/installer | php
```

Установить записимости
```
php composer.phar install
```

Создать MySQL базу:
```sql
CREATE DATABASE printclick CHARACTER SET utf8 COLLATE utf8_unicode_ci;
GRANT USAGE ON printclick.* TO printclick@localhost IDENTIFIED BY 'printclick';
GRANT ALL PRIVILEGES ON printclick.* TO printclick@localhost IDENTIFIED BY 'printclick';
FLUSH PRIVILEGES;
```

Создать новый vhost у Apache:
```
<VirtualHost *:80>
  <Directory "/vagrant/printclick/web">
    AllowOverride all
    Allow from all
    Require all granted
  </Directory>

  ServerAdmin admin@printclick.loc
  DocumentRoot "/vagrant/printclick/web"
  ServerName printclick.loc
  ErrorLog "/var/log/apache2/printclick.loc.log"
  CustomLog "/var/log/apache2/printclick.loc.log" common
</VirtualHost>
```

Добавить информацию в `/etc/hosts`:
```
127.0.0.1      printclick.loc
```

Инициализировать приложение
```
vendor/bin/phing deploy
```

Добавить тестовые данные в БД:
```
./yii data/load
```

Конфигурация
============
Параметры приложения можно переопределить в `build.prop`. Например:
```
env.name                = dev
env.debug               = 1
env.maxtime             = 300
env.maxmemory           = 512M

app.id                  = printclick
app.name                = PrintClick
app.version             = 1.0
app.language            = ru
app.timezone            = Asia/Novosibirsk
app.domain              = printclick.loc

db.dsn                  = mysql:host=localhost;dbname=printclick
db.charset              = UTF8
db.username             = printclick
db.password             = printclick

params.user_login       = vsemayki
params.user_password    = password
params.cookie_key       = abc-def
```
После надо пересобрать проект:
```
vendor/bin/phing deploy
```