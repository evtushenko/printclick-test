<?php
namespace printclick\models;

use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;

/**
 * @property int $id
 * @property int $create_date
 * @property int $category_id
 * @property string $status
 * @property string $slug
 * @property string $title
 * @property string $excerpt
 * @property string $content
 *
 * @property string $statusName
 * @property Category $category
 */
class News extends ActiveRecord
{
    const STATUS_DRAFT  = 'draft';
    const STATUS_ACTIVE = 'active';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class'         => TimestampBehavior::className(),
                'attributes'    => [
                    self::EVENT_BEFORE_INSERT => ['create_date'],
                ],
            ],
            'slug' => [
                'class'         => SluggableBehavior::className(),
                'slugAttribute' => 'slug',
                'ensureUnique'  => true,
                'immutable'     => false,
                'value'         => function() {
                    return $this->generateSlug();
                },
            ],
        ];
    }

    /**
     * @return string
     */
    private function generateSlug()
    {
        static $translit = [
            "Г" => "G",		"Ё" => "YO",	"Є" => "E",		"Ї" => "YI",	"І" => "I",
            "і" => "i",		"ґ" => "g",		"ё" => "yo",	"№" => "#",		"є" => "e",
            "ї" => "yi",	"А" => "A",		"Б" => "B",		"В" => "V",
            "Д" => "D",		"Е" => "E",		"Ж" => "ZH",	"З" => "Z",		"И" => "I",
            "Й" => "Y",		"К" => "K",		"Л" => "L",		"М" => "M",		"Н" => "N",
            "О" => "O",		"П" => "P",		"Р" => "R",		"С" => "S",		"Т" => "T",
            "У" => "U",		"Ф" => "F",		"Х" => "H",		"Ц" => "TS",	"Ч" => "CH",
            "Ш" => "SH",	"Щ" => "SCH",	"Ъ" => "'",		"Ы" => "YI",	"Ь" => "",
            "Э" => "E",		"Ю" => "YU",	"Я" => "YA",	"а" => "a",		"б" => "b",
            "в" => "v",		"г" => "g",		"д" => "d",		"е" => "e",		"ж" => "zh",
            "з" => "z",		"и" => "i",		"й" => "y",		"к" => "k",		"л" => "l",
            "м" => "m",		"н" => "n",		"о" => "o",		"п" => "p",		"р" => "r",
            "с" => "s",		"т" => "t",		"у" => "u",		"ф" => "f",		"х" => "h",
            "ц" => "ts",	"ч" => "ch",	"ш" => "sh",	"щ" => "sch",	"ъ" => "'",
            "ы" => "yi",	"ь" => "",		"э" => "e",		"ю" => "yu",	"я" => "ya"
        ];

        $slug = strtr($this->title, $translit);
        $slug = preg_replace('/[^a-zA-Z0-9=\s—–-]+/u', '', $slug);
        $slug = preg_replace('/[=\s—–-]+/u', '-', $slug);
        $slug = trim($slug, '-');
        $slug = strtolower($slug);

        return $slug;
    }

    /**
     * @return ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return string
     */
    public function getStatusName()
    {
        $list = $this->statusList();
        return isset($list[$this->status]) ? $list[$this->status] : '';
    }

    public static function statusList()
    {
        return [
            self::STATUS_DRAFT => 'Черновик',
            self::STATUS_ACTIVE => 'Опубликована',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'required'],
            ['title', 'trim'],
            ['title', 'string', 'length' => [5, 150]],

            ['excerpt', 'required'],
            ['excerpt', 'trim'],
            ['excerpt', 'string', 'length' => [50, 50000]],

            ['content', 'required'],
            ['content', 'trim'],
            ['content', 'string', 'length' => [50, 1000000]],

            ['category_id', 'required'],
            ['category_id', 'filter', 'filter' => 'intval'],
            ['category_id', 'exist', 'targetClass' => Category::className(), 'targetAttribute' => 'id'],

            ['status', 'required'],
            ['status', 'in', 'range' => array_keys($this->statusList())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title'         => 'Заголовок',
            'excerpt'       => 'Короткое описание',
            'content'       => 'Полный текст',
            'category_id'   => 'Категория',
            'status'        => 'Статус',
            'statusName'    => 'Статус',
        ];
    }

    /**
     * @return NewsQuery
     */
    public static function find()
    {
        return new NewsQuery(get_called_class());
    }
}
