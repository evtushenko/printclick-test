<?php
namespace printclick\models;

use yii\db\ActiveQuery;

/**
 */
class NewsQuery extends ActiveQuery
{
    /**
     * @param string $status
     * @return $this
     */
    public function statusOnly($status)
    {
        return $this->andWhere(['status' => $status]);
    }

    /**
     * @param Category $category
     * @return $this
     */
    public function fromCategory(Category $category)
    {
        $ids = array_map(function($child) {
            return $child['id'];
        }, $category->leaves()->select('id')->asArray(true)->all());
        $ids[] = $category->id;

        return $this->andWhere(['category_id' => $ids]);
    }
}