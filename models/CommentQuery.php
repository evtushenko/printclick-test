<?php
namespace printclick\models;

use yii\db\ActiveQuery;

/**
 */
class CommentQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function orderByDate()
    {
        return $this->orderBy(['create_date' => SORT_DESC]);
    }

    /**
     * @param News $news
     * @return $this
     */
    public function fromNews(News $news)
    {
        return $this->andWhere(['news_id' => $news->id]);
    }
}