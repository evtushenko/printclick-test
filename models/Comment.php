<?php
namespace printclick\models;

use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use yii\behaviors\TimestampBehavior;

/**
 * @property int $id
 * @property int $create_date
 * @property string $text
 * @property int $news_id
 *
 * @property News $news
 */
class Comment extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%comment}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class'         => TimestampBehavior::className(),
                'attributes'    => [
                    self::EVENT_BEFORE_INSERT => ['create_date'],
                ],
            ],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['text', 'required'],
            ['text', 'trim'],
            ['text', 'string', 'length' => [5, 10000]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'text' => 'Текст комментария',
        ];
    }

    /**
     * @return CommentQuery
     */
    public static function find()
    {
        return new CommentQuery(get_called_class());
    }
}
