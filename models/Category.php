<?php
namespace printclick\models;

use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use creocoder\nestedsets\NestedSetsBehavior;

/**
 * @property int $id
 * @property string $name
 * @property int $tree
 * @property int $lft
 * @property int $rgt
 * @property int $dpt
 *
 * @property int $newsCount
 *
 * @method CategoryQuery leaves()
 * @method CategoryQuery parents()
 * @method bool makeRoot()
 * @method bool appendTo()
 * @method bool isChildOf(Category $category)
 * @method bool isRoot()
 * @method bool deleteWithChildren()
 */
class Category extends ActiveRecord
{
    /**
     * @var int
     */
    private $newsCount;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'tree' => [
                'class'             => NestedSetsBehavior::className(),
                'treeAttribute'     => 'tree',
                'leftAttribute'     => 'lft',
                'rightAttribute'    => 'rgt',
                'depthAttribute'    => 'dpt',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @return CategoryQuery
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'trim'],
            ['name', 'string', 'length' => [3, 100]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name'      => 'Название категории',
            'newsCount' => 'Количество новостей',
        ];
    }

    /**
     * @return Category|null
     */
    public function getParent()
    {
        return $this->parents(1)->one();
    }

    /**
     * @return int
     */
    public function getNewsCount()
    {
        if ($this->newsCount === null) {
            $this->newsCount = News::find()
                ->statusOnly(News::STATUS_ACTIVE)
                ->fromCategory($this)
                ->count();
        }

        return $this->newsCount;
    }
}
