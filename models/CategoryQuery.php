<?php
namespace printclick\models;

use yii\db\ActiveQuery;
use creocoder\nestedsets\NestedSetsQueryBehavior;

/**
 * @method $this roots()
 * @method $this leaves()
 */
class CategoryQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }

    /**
     * @return $this
     */
    public function orderByTree()
    {
        return $this->orderBy([
            'tree'  => SORT_ASC,
            'lft'   => SORT_ASC,
        ]);
    }
}