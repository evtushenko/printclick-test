<?php
use yii\db\Migration;

class m150202_181354_add_comment_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%comment}}', [
            'id'            => 'INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'news_id'       => 'INTEGER NOT NULL',
            'create_date'   => 'INTEGER NOT NULL',
            'text'          => 'TEXT NOT NULL',
        ]);
        $this->createIndex('ix__comment__news_id__create_date', '{{%comment}}',
            ['news_id', 'create_date'], false);
        $this->addForeignKey('fk__comment__news_id__news__id',
            '{{%comment}}', 'news_id',
            '{{%news}}', 'id',
            'CASCADE', 'CASCADE');

        return true;
    }

    public function down()
    {
        $this->dropTable('{{%comment}}');

        return true;
    }
}
