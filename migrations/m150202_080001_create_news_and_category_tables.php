<?php
use yii\db\Migration;

class m150202_080001_create_news_and_category_tables extends Migration
{
    public function up()
    {
        $this->createTable('{{%category}}', [
            'id'            => 'INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'name'          => 'VARCHAR(100) NOT NULL',
            'tree'          => 'INTEGER',
            'lft'           => 'INTEGER NOT NULL',
            'rgt'           => 'INTEGER NOT NULL',
            'dpt'           => 'INTEGER NOT NULL',
        ]);
        $this->createIndex('ix__category__tree_lft', '{{%category}}', ['tree', 'lft'], false);

        $this->createTable('{{%news}}', [
            'id'            => 'INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'category_id'   => 'INTEGER NOT NULL',
            'create_date'   => 'INTEGER NOT NULL',
            'title'         => 'VARCHAR(150) NOT NULL',
            'slug'          => 'VARCHAR(150) NOT NULL',
            'status'        => "ENUM('draft', 'active') NOT NULL",
            'excerpt'       => 'TEXT NOT NULL',
            'content'       => 'LONGTEXT NOT NULL',
        ]);
        $this->createIndex('uk__news__slug', '{{%news}}', ['slug'], true);
        $this->createIndex('ix__news__category__id', '{{%news}}', ['category_id'], false);
        $this->addForeignKey('fk__news__category_id__category__id',
            '{{%news}}', 'category_id',
            '{{%category}}', 'id',
            'CASCADE', 'CASCADE');

        return true;
    }

    public function down()
    {
        $this->dropTable('{{%news}}');
        $this->dropTable('{{%category}}');

        return true;
    }
}
