<?php
namespace printclick\widgets;

use yii\base\Widget;
use printclick\models\Category;

/**
 */
class CategoryList extends Widget
{
    /**
     * @var Category
     */
    public $currentCategory;

    /**
     * @inheritdoc
     */
    public function run()
    {
        echo $this->render('CategoryList', [
            'current'       => $this->currentCategory,
            'categories'    => $this->getCategories(),
        ]);
    }

    /**
     * @return Category[]
     */
    private function getCategories()
    {
        $emptyParents = [];

        return array_filter(
            Category::find()->orderByTree()->all(),
            function(Category $category) use (&$emptyParents) {
                foreach ($emptyParents as $emptyParent) {
                    if ($category->isChildOf($emptyParent)) {
                        return false;
                    }
                }

                if ($category->newsCount > 0) {
                    return true;
                } else {
                    $emptyParents[] = $category;
                    return false;
                }
            }
        );
    }
}
