<?php
/**
 * @var yii\web\View $this
 * @var printclick\models\Category[] $categories
 * @var printclick\models\Category|null $current
 */
use yii\helpers\Html;

?>
<ul class="nav nav-pills nav-stacked">
    <?php foreach ($categories as $category): ?>
        <li class="<?= $current !== null && $category->id === $current->id ? 'active' : '' ?>">
            <?= Html::a(
                ($category->dpt ? str_repeat('&mdash;', $category->dpt) . ' ' : '') . Html::encode($category->name),
                ['/news/category', 'id' => $category->id]
            ) ?>
        </li>
    <?php endforeach ?>
</ul>