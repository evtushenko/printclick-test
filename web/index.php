<?php
require(__DIR__ . '/../.env.php');
require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$application = new yii\web\Application(require(__DIR__ . '/../config/web.php'));
$application->run();
