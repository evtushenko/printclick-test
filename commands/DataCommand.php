<?php
namespace printclick\commands;

use RuntimeException;
use InvalidArgumentException;
use Yii;
use yii\console\Controller;
use printclick\models\News;
use printclick\models\Category;
use Faker\Factory;
use Faker\Generator;

/**
 */
class DataCommand extends Controller
{
    const MIN_NEWS              = 80;
    const MAX_NEWS              = 100;
    const MAX_CATEGORY_DEPTH    = 3;
    const MIN_CATEGORY_CHILDREN = 1;
    const MAX_CATEGORY_CHILDREN = 3;

    /**
     * @var Generator
     */
    private $faker;

    /**
     * @return void
     * @throws RuntimeException
     */
    public function actionLoad()
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $categories = $this->generateCategories();
            $this->generateNews($categories);

            $transaction->commit();
        } catch (RuntimeException $e) {
            $transaction->rollBack();

            throw $e;
        }
    }

    /**
     * @return Generator
     */
    private function getFaker()
    {
        if ($this->faker === null) {
            $this->faker = Factory::create(Yii::$app->language);
        }

        return $this->faker;
    }

    /**
     * @param Category $parent
     * @param int $depth
     * @return Category[]
     */
    private function generateCategories(Category $parent = null, $depth = 0)
    {
        if ($depth >= self::MAX_CATEGORY_DEPTH) {
            return [];
        }

        $result = [];
        for ($i = 0, $n = $this->getFaker()->numberBetween(self::MIN_CATEGORY_CHILDREN, self::MAX_CATEGORY_CHILDREN); $i < $n; ++$i) {
            $category       = new Category();
            $category->name = $this->getFaker()->name;

            $success = $parent === null ? $category->makeRoot() : $category->appendTo($parent);
            if (!$success) {
                throw new RuntimeException(sprintf('Невозможно создать категорию: %s',
                    json_encode($category->errors, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)));
            }

            $result = array_merge($result, [$category], $this->generateCategories($category, $depth + 1));
        }

        return $result;
    }

    /**
     * @param Category[] $categories
     * @return News[]
     */
    private function generateNews(array $categories)
    {
        if (empty($categories)) {
            throw new InvalidArgumentException('Невозможно выбрать случайную категорию');
        }

        $result = [];
        for ($i = 0, $n = $this->getFaker()->numberBetween(self::MIN_NEWS, self::MAX_NEWS); $i < $n; ++$i) {
            $news               = new News();
            $news->title        = $this->getFaker()->sentence();
            $news->category_id  = $this->randomCategory($categories)->id;
            $news->status       = $this->getFaker()->randomDigit % 4 === 0 ? News::STATUS_DRAFT : News::STATUS_ACTIVE;
            $news->excerpt      = $this->getFaker()->realText(500);
            $news->content      = $this->getFaker()->realText(5000);

            if (!$news->save()) {
                throw new RuntimeException(sprintf('Невозможно создать новость: %s',
                    json_encode($news->errors, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)));
            }

            $result[] = $news;
        }

        return $result;
    }

    /**
     * @param Category[] $categories
     * @return Category
     */
    private function randomCategory(array $categories)
    {
        return $this->getFaker()->randomElement($categories);
    }
}
