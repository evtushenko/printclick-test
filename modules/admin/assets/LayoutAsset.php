<?php
namespace printclick\modules\admin\assets;

use yii\web\AssetBundle;

class LayoutAsset extends AssetBundle
{
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}