<?php
namespace printclick\modules\admin;

use Yii;
use yii\base\Module as BaseModule;

/**
 */
class Module extends BaseModule
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        foreach ($this->components as $id => $component) {
            Yii::$app->set($id, $component);
        }

        parent::init();
    }
}
