<?php
namespace printclick\modules\admin\models;

use Yii;
use yii\base\Model;

/**
 */
class LoginForm extends Model
{
    /**
     * @var string
     */
    public $username;
    /**
     * @var string
     */
    public $password;
    /**
     * @var User|null
     */
    private $user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username'  => 'Имя пользователя',
            'password'  => 'Пароль',
        ];
    }

    /**
     * @param string $attribute
     */
    public function validatePassword($attribute)
    {
        if ($this->hasErrors()) {
            return;
        }

        $user = $this->getUser();
        if (!$user || !$user->validatePassword($this->{$attribute})) {
            $this->addError($attribute, 'Неверный пароль');
        }
    }

    /**
     * @return bool
     */
    public function login()
    {
        return $this->validate() && Yii::$app->user->login($this->getUser());
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        if ($this->user === false) {
            $this->user = User::findByUsername($this->username);
        }

        return $this->user;
    }
}