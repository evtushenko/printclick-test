<?php
namespace printclick\modules\admin\models;

use Yii;
use yii\base\Object;
use yii\web\IdentityInterface;

class User extends Object implements IdentityInterface
{
    /**
     * @var string
     */
    public $id;
    /**
     * @var string
     */
    public $username;
    /**
     * @var string
     */
    public $password;
    /**
     * @var string
     */
    public $authkey;
    /**
     * @var string
     */
    public $token;

    /**
     * @return array
     */
    private static function getUserParams()
    {
        return [
            'id'            => '100',
            'username'      => Yii::$app->params['user.login'],
            'password'      => Yii::$app->params['user.password'],
            'authkey'       => __CLASS__,
            'token'         => __FILE__,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $user = self::getUserParams();

        return $user['id'] === $id ? new static($user) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $user = self::getUserParams();

        return $user['token'] === $token ? new static($user) : null;
    }

    /**
     * @param string $username
     * @return User|null
     */
    public static function findByUsername($username)
    {
        $user = self::getUserParams();

        return $user['username'] === $username ? new static($user) : null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authkey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authkey === $authKey;
    }

    /**
     * @param string $password
     * @return bool
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
