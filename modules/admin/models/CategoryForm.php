<?php
namespace printclick\modules\admin\models;

use yii\base\Model;
use printclick\models\Category;

/**
 */
class CategoryForm extends Model
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var int
     */
    public $parent_id;

    /**
     * @var Category
     */
    private $category;

    /**
     * @param Category $category
     * @param array $config
     */
    public function __construct(Category $category, array $config = [])
    {
        $this->category = $category;
        $this->setAttributes([
            'name'      => $category->name,
            'parent_id' => ($parent = $category->getParent()) !== null ? $parent->id : null,
        ]);

        parent::__construct($config);
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'trim'],
            ['name', 'string', 'length' => [3, 100]],

            ['parent_id', 'filter', 'filter' => function($v) { return (int)$v ?: null; }],
            ['parent_id', 'exist', 'targetClass' => Category::className(), 'targetAttribute' => 'id']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name'      => 'Название категории',
            'parent_id' => 'Родительская категория',
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        $this->category->setAttributes(['name' => $this->name]);

        $parent = $this->parent_id === null ? null : Category::findOne($this->parent_id);
        if ($parent === null) {
            return $this->category->isRoot() ? $this->category->save() : $this->category->makeRoot();
        } else {
            return $this->category->appendTo($parent);
        }
    }
}
