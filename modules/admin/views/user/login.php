<?php
/**
 * @var yii\web\View $this
 * @var printclick\modules\admin\models\LoginForm $model
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Вход';
?>
<div class="user-login">
    <h1 class="text-center">Вход</h1>
    <div class="alert alert-info">
        Для входа нужно использовать
        <b><?= Yii::$app->params['user.login'] ?></b> / <b><?= Yii::$app->params['user.password'] ?></b>.
    </div>

    <?php
    $form = ActiveForm::begin([
        'options' => [
            'class' => 'form-vertical',
        ],
    ]);

    echo $form->field($model, 'username')->textInput();
    echo $form->field($model, 'password')->passwordInput();
    ?>

    <div class="form-actions">
        <?= Html::submitButton('Войти', ['class' => 'btn btn-primary btn-block']) ?>
    </div>

    <?php ActiveForm::end() ?>
</div>