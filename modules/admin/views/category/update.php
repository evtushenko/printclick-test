<?php
/**
 * @var yii\web\View $this
 * @var printclick\modules\admin\models\CategoryForm $model
 * @var printclick\models\Category[] $list
 */

$this->title    = sprintf('Изменить категорию "%s"', $model->getCategory()->name);
$this->params   = [
    'breadcrumbs'   => [
        [
            'label' => 'Категории',
            'url'   => ['list'],
        ],
        [
            'label' => $model->getCategory()->name,
            'url'   => ['view', 'id' => $model->getCategory()->id],
        ],
        [
            'label' => 'Редактировать',
        ],
    ],
];
?>
<div id="category-update">
    <?= $this->render('_form', ['model' => $model, 'list' => $list]) ?>
</div>