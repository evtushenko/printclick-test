<?php
/**
 * @var yii\web\View $this
 * @var printclick\modules\admin\models\CategoryForm $model
 * @var printclick\models\Category[] $list
 */

$this->title    = 'Новая категория';
$this->params   = [
    'breadcrumbs'   => [
        [
            'label' => 'Категории',
            'url'   => ['list'],
        ],
        [
            'label' => 'Новая',
        ],
    ],
];
?>
<div id="category-create">
    <?= $this->render('_form', ['model' => $model, 'list' => $list]) ?>
</div>