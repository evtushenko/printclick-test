<?php
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $categories
 */
use yii\grid\GridView;
use yii\grid\DataColumn;
use yii\helpers\Html;
use printclick\models\Category;

$this->title    = 'Категории';
$this->params   = [
    'breadcrumbs'   => [
        [
            'label' => 'Категории',
        ],
    ],
    'actions'       => [
        [
            'label' => 'Создать',
            'url'   => ['create'],
        ],
    ],
];
?>
<div id="category-list">
    <?= GridView::widget([
        'dataProvider'  => $categories,
        'columns'       => [
            [
                'class'     => DataColumn::className(),
                'label'     => 'Название',
                'format'    => 'raw',
                'value'     => function(Category $category) {
                    return ($category->dpt ? str_repeat('—', $category->dpt) . ' ' : '') .
                        Html::a(Html::encode($category->name), ['view', 'id' => $category->id]);
                },
            ],
            [
                'class'     => DataColumn::className(),
                'label'     => 'Количество новостей',
                'format'    => 'raw',
                'value'     => function(Category $category) {
                    return $category->newsCount;
                },
            ],
        ],
    ]) ?>
</div>