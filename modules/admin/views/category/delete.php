<?php
/**
 * @var yii\web\View $this
 * @var printclick\models\Category $category
 */
use yii\helpers\Html;

$this->title    = sprintf('Удалить категорию "%s"', $category->name);
$this->params   = [
    'breadcrumbs'   => [
        [
            'label' => 'Категории',
            'url'   => ['list'],
        ],
        [
            'label' => $category->name,
            'url'   => ['view', 'id' => $category->id],
        ],
        [
            'label' => 'Удалить',
        ],
    ],
];
?>
<div id="category-delete">
    <?= Html::beginForm() ?>

    <div class="alert alert-warning">
        <h4>Внимание!</h4>
        <p>Вместе с категорией будут удалены все дочерние категории и все новости, привязанные к ним!</p>
    </div>

    <div class="form-actions">
        <?= Html::submitButton('Удалить категорию', ['class' => 'btn btn-danger']) ?>
    </div>

    <?= Html::endForm() ?>
</div>