<?php
/**
 * @var yii\web\View $this
 * @var printclick\models\Category $category
 */
use yii\widgets\DetailView;
use yii\helpers\Html;
use printclick\models\Category;

$this->title    = $category->name;
$this->params   = [
    'breadcrumbs'   => [
        [
            'label' => 'Категории',
            'url'   => ['list'],
        ],
        [
            'label' => $category->name,
        ],
    ],
    'actions'       => [
        [
            'label' => 'Редактировать',
            'url'   => ['update', 'id' => $category->id],
        ],
        [
            'label' => 'Удалить',
            'url'   => ['delete', 'id' => $category->id],
        ],
    ],
];
?>
<div id="category-view">
    <?= DetailView::widget([
        'model'         => $category,
        'attributes'    => [
            'name',
            'newsCount',
            [
                'label'     => 'Родители',
                'format'    => 'raw',
                'value'     => implode(array_map(function(Category $category) {
                    return Html::a(Html::encode($category->name), ['view', 'id' => $category->id]);
                }, $category->parents()->all()), ' » '),
            ],
        ],
    ]) ?>
</div>