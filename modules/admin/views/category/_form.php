<?php
/**
 * @var yii\web\View $this
 * @var printclick\modules\admin\models\CategoryForm $model
 * @var printclick\models\Category[] $list
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$dropDown = [];
foreach ($list as $item) {
    if ($item->id === $model->getCategory()->id || $item->isChildOf($model->getCategory())) {
        continue;
    }

    $dropDown[$item->id] = ($item->dpt ? str_repeat('—', $item->dpt) . ' ' : '') .
        $item->name;
}
?>
<div id="category-form">
    <?php
    $form = ActiveForm::begin([
        'options' => [
            'class' => 'form-vertical',
        ],
    ]);

    echo $form->field($model, 'name')->textInput();
    echo $form->field($model, 'parent_id')->dropDownList($dropDown, ['prompt' => '- нет -']);
    ?>

    <div class="form-actions">
        <?= Html::submitButton($model->getCategory()->isNewRecord ? 'Создать' : 'Сохранить',
            ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>