<?php
/**
 * @var yii\web\View $this
 */
use yii\helpers\Html;

$this->title = 'Администрирование';
?>
<div id="default-index">
    <div class="row">
        <div class="col-sm-6">
            <div class="jumbotron">
                <h1>Категории</h1>
                <p>
                    <?= Html::a('Смотреть', ['category/list'], ['class' => 'btn btn-lg btn-primary']) ?>
                    <?= Html::a('Добавить', ['category/create'], ['class' => 'btn btn-lg btn-success']) ?>
                </p>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="jumbotron">
                <h1>Новости</h1>
                <p>
                    <?= Html::a('Смотреть', ['news/list'], ['class' => 'btn btn-lg btn-primary']) ?>
                    <?= Html::a('Добавить', ['news/create'], ['class' => 'btn btn-lg btn-success']) ?>
                </p>
            </div>
        </div>
    </div>
</div>