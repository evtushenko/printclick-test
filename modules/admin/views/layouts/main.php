<?php
/**
 * @var yii\web\View $this
 * @var string $content
 */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\bootstrap\ButtonDropdown;
use yii\widgets\Breadcrumbs;
use printclick\modules\admin\assets\LayoutAsset;

LayoutAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel'    => 'Администрирование',
        'brandUrl'      => ['/admin'],
    ]);

    echo Nav::widget([
        'options'   => [
            'class' => 'navbar-nav navbar-left',
        ],
        'items'     => [
            [
                'label'     => 'Категории',
                'url'       => ['category/list'],
                'active'    => Yii::$app->controller->id === 'category',
            ],
            [
                'label'     => 'Новости',
                'url'       => ['news/list'],
                'active'    => Yii::$app->controller->id === 'news',
            ],
        ],
    ]);

    echo Nav::widget([
        'options'   => [
            'class' => 'navbar-nav navbar-right',
        ],
        'items'     => [
            [
                'label'         => 'Выход',
                'url'           => ['user/logout'],
                'linkOptions'   => ['data-method' => 'post'],
            ],
        ],
    ]);

    NavBar::end();
    ?>

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-sm-8">
                <?= Breadcrumbs::widget([
                    'homeLink'  => false,
                    'links'     => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </div>

            <?php if (!empty($this->params['actions'])): ?>
                <div class="col-md-2 col-sm-4">
                    <?= ButtonDropdown::widget([
                        'label'             => 'Действия',
                        'options'           => ['class' => 'btn btn-default'],
                        'containerOptions'  => ['class' => 'pull-right'],
                        'dropdown'          => ['items' => $this->params['actions']],
                    ]) ?>
                </div>
            <?php endif ?>
        </div>
    </div>

    <div class="container">
        <h1 class="page-header" style="margin-top: 0"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="container">
        <?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
