<?php
/**
 * @var yii\web\View $this
 * @var printclick\models\News $news
 * @var printclick\models\Category[] $categories
 */

$this->title    = 'Добавить новость';
$this->params   = [
    'breadcrumbs'   => [
        [
            'label' => 'Новости',
            'url'   => ['list'],
        ],
        [
            'label' => 'Новая',
        ],
    ],
];
?>
<div id="news-create">
    <?= $this->render('_form', ['news' => $news, 'categories' => $categories]) ?>
</div>