<?php
/**
 * @var yii\web\View $this
 * @var printclick\models\News $news
 */
use yii\widgets\DetailView;
use yii\helpers\Html;

$this->title    = $news->title;
$this->params   = [
    'breadcrumbs'   => [
        [
            'label' => 'Новости',
            'url'   => ['list'],
        ],
        [
            'label' => $news->title,
        ],
    ],
    'actions'       => [
        [
            'label' => 'Редактировать',
            'url'   => ['update', 'id' => $news->id],
        ],
        [
            'label' => 'Удалить',
            'url'   => ['delete', 'id' => $news->id],
        ],
    ],
];
?>
<div id="news-view">
    <?= DetailView::widget([
        'model'         => $news,
        'attributes'    => [
            'title',
            'statusName',
            [
                'label'     => 'URL',
                'value'     => Yii::$app->urlManager->createAbsoluteUrl(['/news/view', 'slug' => $news->slug]),
                'format'    => 'url',
            ],
            [
                'label'     => 'Категория',
                'value'     => Html::a(Html::encode($news->category->name), ['category/view', 'id' => $news->category->id]),
                'format'    => 'raw',
            ],
            'excerpt:ntext',
            'content:ntext',
        ],
    ]) ?>
</div>