<?php
/**
 * @var yii\web\View $this
 * @var printclick\models\News $news
 * @var printclick\models\Category[] $categories
 */

$this->title    = sprintf('Изменить новость "%s"', $news->title);
$this->params   = [
    'breadcrumbs'   => [
        [
            'label' => 'Новости',
            'url'   => ['list'],
        ],
        [
            'label' => $news->title,
            'url'   => ['view', 'id' => $news->id],
        ],
        [
            'label' => 'Редактировать',
        ],
    ],
];
?>
<div id="news-update">
    <?= $this->render('_form', ['news' => $news, 'categories' => $categories]) ?>
</div>