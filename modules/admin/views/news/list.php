<?php
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $news
 */
use yii\widgets\ListView;

$this->title    = 'Новости';
$this->params   = [
    'breadcrumbs'   => [
        [
            'label' => 'Новости',
        ],
    ],
    'actions'       => [
        [
            'label' => 'Создать',
            'url'   => ['create'],
        ],
    ],
];
?>
<div id="news-list">
    <?= ListView::widget([
        'dataProvider'  => $news,
        'itemView'      => '_item',
    ]) ?>
</div>