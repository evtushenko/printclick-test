<?php
/**
 * @var yii\web\View $this
 * @var printclick\models\News $news
 */
use yii\helpers\Html;

$this->title    = sprintf('Удалить новость "%s"', $news->title);
$this->params   = [
    'breadcrumbs'   => [
        [
            'label' => 'Новости',
            'url'   => ['list'],
        ],
        [
            'label' => $news->title,
            'url'   => ['view', 'id' => $news->id],
        ],
        [
            'label' => 'Удалить',
        ],
    ],
];
?>
<div id="news-delete">
    <?= Html::beginForm() ?>

    <div class="alert alert-warning">
        <h4>Внимание!</h4>
        <p>Новость будет удалена безвозвратно!</p>
    </div>

    <div class="form-actions">
        <?= Html::submitButton('Удалить новость', ['class' => 'btn btn-danger']) ?>
    </div>

    <?= Html::endForm() ?>
</div>