<?php
/**
 * @var yii\web\View $this
 * @var printclick\models\News $model
 */
use yii\helpers\Html;
use printclick\models\News;

?>
<div class="media news-item">
    <div class="media-body">
        <h3 class="media-heading"><?= Html::a(Html::encode($model->title), ['view', 'id' => $model->id]) ?></h3>
        <p>
            <i><span class="label <?= $model->status === News::STATUS_DRAFT ? 'label-default' : 'label-success' ?>"><?= $model->statusName ?></span></i>
            <i>в <?= Html::a(Html::encode($model->category->name), ['category/view', 'id' => $model->category->id])?></i>
            <i>от <?= Yii::$app->formatter->asDatetime($model->create_date) ?></i>
        </p>
        <p><?= Yii::$app->formatter->asNtext($model->excerpt) ?></p>
    </div>
</div>