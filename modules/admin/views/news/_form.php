<?php
/**
 * @var yii\web\View $this
 * @var printclick\models\News $news
 * @var printclick\models\Category[] $categories
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$dropDown = [];
foreach ($categories as $item) {
    $dropDown[$item->id] = ($item->dpt ? str_repeat('—', $item->dpt) . ' ' : '') .
        $item->name;
}
?>
<div id="news-form">
    <?php
    $form = ActiveForm::begin([
        'options' => [
            'class' => 'form-vertical',
        ],
    ]);

    echo $form->field($news, 'title')->textInput();
    echo $form->field($news, 'category_id')->dropDownList($dropDown, ['prompt' => '- выбрать -']);
    echo $form->field($news, 'status')->dropDownList($news->statusList());
    echo $form->field($news, 'excerpt')->textarea(['rows' => 3]);
    echo $form->field($news, 'content')->textarea(['rows' => 15]);
    ?>

    <div class="form-actions">
        <?= Html::submitButton($news->isNewRecord ? 'Создать' : 'Сохранить',
            ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>