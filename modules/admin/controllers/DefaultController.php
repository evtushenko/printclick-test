<?php
namespace printclick\modules\admin\controllers;

use yii\web\Controller;
use yii\filters\AccessControl;

/**
 */
class DefaultController extends Controller
{
    public $layout = 'main';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'     => true,
                        'actions'   => ['index'],
                        'roles'     => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}
