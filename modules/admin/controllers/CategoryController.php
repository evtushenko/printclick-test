<?php
namespace printclick\modules\admin\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use yii\filters\AccessControl;
use printclick\models\Category;
use printclick\modules\admin\models\CategoryForm;

/**
 */
class CategoryController extends Controller
{
    public $layout = 'main';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'     => true,
                        'actions'   => ['list', 'view', 'create', 'update', 'delete'],
                        'roles'     => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionList()
    {
        $categories = new ActiveDataProvider([
            'query'         => Category::find()->orderByTree(),
            'pagination'    => false,
        ]);

        return $this->render('list', ['categories' => $categories]);
    }

    public function actionView($id)
    {
        $category = Category::findOne($id);
        if ($category === null) {
            throw new NotFoundHttpException('Категория не найдена');
        }

        return $this->render('view', ['category' => $category]);
    }

    public function actionCreate()
    {
        $category = new Category();

        $form = new CategoryForm($category);
        if ($form->load(Yii::$app->request->post()) && $form->save()) {
            return $this->redirect(['view', 'id' => $category->id]);
        }

        return $this->render('create', [
            'model' => $form,
            'list'  => Category::find()->orderByTree()->all(),
        ]);
    }

    public function actionUpdate($id)
    {
        $category = Category::findOne($id);
        if ($category === null) {
            throw new NotFoundHttpException('Категория не найдена');
        }

        $form = new CategoryForm($category);
        if ($form->load(Yii::$app->request->post()) && $form->save()) {
            return $this->redirect(['view', 'id' => $category->id]);
        }

        return $this->render('update', [
            'model' => $form,
            'list'  => Category::find()->orderByTree()->all(),
        ]);
    }

    public function actionDelete($id)
    {
        $category = Category::findOne($id);
        if ($category === null) {
            throw new NotFoundHttpException('Категория не найдена');
        }

        if (Yii::$app->request->isPost) {
            if (!$category->deleteWithChildren()) {
                throw new ServerErrorHttpException('Невозможно удалить категорию');
            }

            return $this->redirect(['list']);
        }

        return $this->render('delete', ['category' => $category]);
    }
}
