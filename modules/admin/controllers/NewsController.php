<?php
namespace printclick\modules\admin\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use yii\filters\AccessControl;
use printclick\models\Category;
use printclick\models\News;

/**
 */
class NewsController extends Controller
{
    public $layout = 'main';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'     => true,
                        'actions'   => ['list', 'view', 'create', 'update', 'delete'],
                        'roles'     => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionList()
    {
        $news = new ActiveDataProvider([
            'query'         => News::find(),
            'pagination'    => false,
        ]);

        return $this->render('list', ['news' => $news]);
    }

    public function actionView($id)
    {
        $news = News::findOne($id);
        if ($news === null) {
            throw new NotFoundHttpException('Новость не найдена');
        }

        return $this->render('view', ['news' => $news]);
    }

    public function actionCreate()
    {
        $news = new News();

        if ($news->load(Yii::$app->request->post()) && $news->save()) {
            return $this->redirect(['view', 'id' => $news->id]);
        }

        return $this->render('create', [
            'news'          => $news,
            'categories'    => Category::find()->orderByTree()->all(),
        ]);
    }

    public function actionUpdate($id)
    {
        $news = News::findOne($id);
        if ($news === null) {
            throw new NotFoundHttpException('Новость не найдена');
        }

        if ($news->load(Yii::$app->request->post()) && $news->save()) {
            return $this->redirect(['view', 'id' => $news->id]);
        }

        return $this->render('update', [
            'news'          => $news,
            'categories'    => Category::find()->orderByTree()->all(),
        ]);
    }

    public function actionDelete($id)
    {
        $news = News::findOne($id);
        if ($news === null) {
            throw new NotFoundHttpException('Новость не найдена');
        }

        if (Yii::$app->request->isPost) {
            if (!$news->delete()) {
                throw new ServerErrorHttpException('Невозможно удалить новость');
            }

            return $this->redirect(['list']);
        }

        return $this->render('delete', ['news' => $news]);
    }
}
