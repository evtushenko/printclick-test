<?php
namespace printclick\controllers;

use Yii;
use yii\web\Controller;

class SiteController extends Controller
{
    public $layout = 'main';

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
}
