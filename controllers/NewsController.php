<?php
namespace printclick\controllers;

use printclick\models\Comment;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use printclick\models\News;
use printclick\models\Category;

class NewsController extends Controller
{
    public $layout = '2column';

    public function actionIndex()
    {
        $news = new ActiveDataProvider([
            'query'         => News::find()->statusOnly(News::STATUS_ACTIVE),
            'pagination'    => [
                'pageParam'         => 'p',
                'pageSize'          => 3,
                'defaultPageSize'   => 3,
            ],
            'sort'          => [
                'sortParam'     => 's',
                'defaultOrder'  => ['date' => SORT_DESC],
                'attributes'    => [
                    'date' => [
                        'asc'       => ['create_date' => SORT_ASC],
                        'desc'      => ['create_date' => SORT_DESC],
                        'default'   => SORT_ASC,
                        'label'     => 'Дата',
                    ],
                ]
            ],
        ]);

        return $this->render('index', ['news' => $news]);
    }

    public function actionCategory($id)
    {
        $category = Category::findOne($id);
        if ($category === null) {
            throw new NotFoundHttpException('Новость не найдена');
        }

        $news = new ActiveDataProvider([
            'query'         => News::find()->statusOnly(News::STATUS_ACTIVE)->fromCategory($category),
            'pagination'    => [
                'pageParam'         => 'p',
                'pageSize'          => 3,
                'defaultPageSize'   => 3,
            ],
            'sort'          => [
                'sortParam'     => 's',
                'defaultOrder'  => ['date' => SORT_DESC],
                'attributes'    => [
                    'date' => [
                        'asc'       => ['create_date' => SORT_ASC],
                        'desc'      => ['create_date' => SORT_DESC],
                        'default'   => SORT_ASC,
                        'label'     => 'Дата',
                    ],
                ]
            ],
        ]);

        return $this->render('category', ['news' => $news, 'category' => $category]);
    }

    public function actionView($slug)
    {
        $news = News::findOne(['slug' => $slug, 'status' => News::STATUS_ACTIVE]);
        if ($news === null) {
            throw new NotFoundHttpException('Новость не найдена');
        }

        $comments = new ActiveDataProvider([
            'query'         => Comment::find()->fromNews($news)->orderByDate(),
            'pagination'    => false,
            'sort'          => false,
        ]);

        return $this->render('view', ['news' => $news, 'comments' => $comments]);
    }

    public function actionComment($id)
    {
        $news = News::findOne(['id' => $id, 'status' => News::STATUS_ACTIVE]);
        if ($news === null) {
            throw new NotFoundHttpException('Новость не найдена');
        }

        $comment            = new Comment();
        $comment->news_id   = $news->id;

        if ($comment->load(Yii::$app->request->post()) && $comment->save()) {
            return $this->redirect(['view', 'slug' => $news->slug]);
        }

        throw new BadRequestHttpException('Некорректные параметры комментария');
    }
}
