<?php
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $news
 */
use yii\widgets\ListView;

$this->title = 'Все новости';
?>
<div id="news-list">
    <h1 class="page-header">Все новости</h1>
    <?= ListView::widget([
        'dataProvider'  => $news,
        'itemView'      => '_item',
        'sorter'        => [
            'options' => ['class' => 'list-inline', 'style' => 'display: inline-block']
        ],
        'layout'        => <<<HTML
<div class="pull-left">{summary}</div>
<div class="pull-right">Сортировать по: {sorter}</div>
<div class="clearfix"></div>
{items}
{pager}
HTML
    ]) ?>
</div>