<?php
/**
 * @var yii\web\View $this
 * @var printclick\models\News $model
 */
use yii\helpers\Html;

?>
<div class="media news-item">
    <div class="media-body">
        <h3 class="media-heading"><?= Html::a(Html::encode($model->title), ['view', 'slug' => $model->slug]) ?></h3>
        <p>
            <i>в &laquo;<?= Html::a(Html::encode($model->category->name), ['category', 'id' => $model->category->id]) ?>&raquo;</i>
            <i>от <?= Yii::$app->formatter->asDatetime($model->create_date) ?></i>
        </p>
        <p><?= Yii::$app->formatter->asNtext($model->excerpt) ?></p>
    </div>
</div>