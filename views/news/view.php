<?php
/**
 * @var yii\web\View $this
 * @var printclick\models\News $news
 * @var yii\data\ActiveDataProvider $comments
 */
use yii\helpers\Html;
use yii\widgets\ListView;
use printclick\models\Comment;

$this->title = $news->title;
$this->params['currentCategory'] = $news->category;
?>
<div id="news-view">
    <h1 class="page-header"><?= Html::encode($news->title) ?></h1>
    <p>
        <i>в &laquo;<?= Html::a(Html::encode($news->category->name), ['category', 'id' => $news->category->id]) ?>&raquo;</i>
        <i>от <?= Yii::$app->formatter->asDatetime($news->create_date) ?></i>
    </p>
    <p><?= Yii::$app->formatter->asNtext($news->content) ?></p>

    <hr />
    <h2>Комментарии</h2>
    <?= $this->render('comment/_form', ['news' => $news, 'comment' => new Comment()]) ?>

    <div style="height: 20px;"></div>
    <?= ListView::widget([
        'dataProvider'      => $comments,
        'itemView'          => 'comment/_item',
        'emptyText'         => 'Пока комментариев нет',
        'emptyTextOptions'  => ['class' => 'alert alert-warning'],
        'layout'            => '{items}',
    ]) ?>
</div>