<?php
/**
 * @var yii\web\View $this
 * @var printclick\models\News $news
 * @var printclick\models\Comment $comment
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>
<div id="comment-form">
    <?php $form = ActiveForm::begin(['action' => ['comment', 'id' => $news->id]]) ?>

    <?= $form->field($comment, 'text')->textarea(['rows' => 3])?>

    <div class="form-actions">
        <?= Html::submitButton('Добавить комментарий', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>