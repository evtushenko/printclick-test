<?php
/**
 * @var yii\web\View $this
 * @var printclick\models\Comment $model
 */
?>
<div class="media comment-item">
    <div class="media-body">
        <p><i><?= Yii::$app->formatter->asDatetime($model->create_date) ?></i></p>
        <p><?= Yii::$app->formatter->asNtext($model->text) ?></p>
    </div>
</div>