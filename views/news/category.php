<?php
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $news
 * @var printclick\models\Category $category
 */
use yii\widgets\ListView;
use yii\helpers\Html;

$this->title = sprintf('Новости из категории "%s"', $category->name);
$this->params['currentCategory'] = $category;
?>
<div id="news-category">
    <h1 class="page-header"><?= Html::encode($category->name) ?></h1>
    <?= ListView::widget([
        'dataProvider'  => $news,
        'itemView'      => '_item',
        'sorter'        => [
            'options' => ['class' => 'list-inline', 'style' => 'display: inline-block']
        ],
        'layout'        => <<<HTML
<div class="pull-left">{summary}</div>
<div class="pull-right">Сортировать по: {sorter}</div>
<div class="clearfix"></div>
{items}
{pager}
HTML
    ]) ?>
</div>