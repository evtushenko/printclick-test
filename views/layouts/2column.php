<?php
/**
 * @var yii\web\View $this
 * @var string $content
 */
use printclick\widgets\CategoryList;

?>
<?php $this->beginContent('@app/views/layouts/main.php') ?>
<div class="row">
    <div class="col-md-9 col-sm-8">
        <?= $content ?>
    </div>
    <div class="col-md-3 col-sm-4">
        <h3>Категории</h3>
        <?= CategoryList::widget([
            'currentCategory' => isset($this->params['currentCategory']) ? $this->params['currentCategory'] : null,
        ]) ?>
    </div>
</div>
<?php $this->endContent() ?>